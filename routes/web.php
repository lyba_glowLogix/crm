<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('company', function (){
    return view('company');})->name('company-list');
Route::get('employee', function (){
    return view('employee');})->name('employee-list');
require __DIR__.'/auth.php';
//company Routes

Route::get('company-list',[CompanyController::class,'index'])->name('company-list');
Route::get('company-add',[CompanyController::class,'create'])->name('company-add');
Route::post('company-store',[CompanyController::class,'store'])->name('company-store');
Route::get('company-edit/{id}',[CompanyController::class,'edit'])->name('company-edit');
Route::post('company-update',[CompanyController::class,'update'])->name('company-store');
Route::get('company-delete/{id}',[CompanyController::class,'delete'])->name('company-delete');

//Employee Routes
Route::get('employee-list',[EmployeeController::class,'index'])->name('employee-list');
Route::get('employee-add',[EmployeeController::class,'create'])->name('employee-add');
Route::post('employee-store',[EmployeeController::class,'store'])->name('employee-store');
Route::get('employee-edit/{id}',[EmployeeController::class,'edit'])->name('employee-edit');
Route::post('employee-update',[EmployeeController::class,'update'])->name('employee-store');
Route::get('employee-delete/{id}',[EmployeeController::class,'delete'])->name('employee-delete');
