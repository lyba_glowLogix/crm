<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index(){
        $members = Employee::get();
        $data = Company::get();

       return view('employee',compact('members','data'));
    }
    public function create(){
        $data = Company::get();
        return view('employees.create-employee',compact('data'));
    }
    public function store(Request $request){
        $rules = [
            'first_name' => ['required'],
            'last_name'=> ['required'],
            'email'=>['required'],
            'phone'=>['required'],
        ];
        $messages = [];

        $request->validate($rules, $messages);
        $first_name = $request->first_name;
        $last_name=$request->last_name;
        $comp_id=$request->comp_id;
        $email=$request->email;
        $phone=$request->phone;
        $employee = new Employee();
        $employee->first_name = $first_name;
        $employee->last_name = $last_name;
        $employee->comp_id = $comp_id;
        $employee->email = $email;
        $employee->phone = $phone;
        $employee->save();
        return redirect()->back()->with('success', 'Record Added Successfully!!!');
    }
    public function edit($id){
        $data = Company::get();
        $members= Employee::where('id','=',$id)->first();
        return view('employees.edit-employee',compact('members','data'));
    }
    public function update(Request $request){
        $data = Company::get();
         $id= $request->id;
        $first_name = $request->first_name;
        $last_name=$request->last_name;
        $comp_id=$request->comp_id;
        $email=$request->email;
        $phone=$request->phone;

       Employee::where('id','=',$id)->update([
            'first_name'=>$first_name,
           'last_name'=>$last_name,
           'comp_id'=>$comp_id,
           'email'=>$email,
           'phone'=>$phone,
        ]);
        return redirect()->back()->with('success', 'Record Updated Successfully!!!');

    }
    public function delete($id){
        Employee::where('id','=',$id)->delete();
        return redirect()->back()->with('success', 'Record Deleted Successfully!!!');
    }
}
