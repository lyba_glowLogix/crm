<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{

    public function index(){
      $data = Company::get();
      //return $data;
        return view('company',compact('data'));
    }
    public function create(){
        return view('company.create-company');
    }
    public function store(Request $request){
        $rules = [
            'name' => ['required'],
            'email'=> ['required'],
            'logo'=>['mimes:jpeg,png,jpg,gif,svg','max:2048'],
        ];
        $messages = [];

        $request->validate($rules, $messages);
        $company = new Company();
        $company->name = $request->name;
        $company->email=$request->email;
       //------------------------//
        //upload an image
        $logo=$request->logo;
        if ($logo!=null){
            $imagename= $logo->getCLientOriginalName();

            $logo->storeAs('public/images',$imagename);
            $company->logo = $imagename;
        }
        //------------------------//
        $company->website=$request->website;
        $company->save();
        return redirect()->back()->with('success', 'Record Added Successfully!!!');

    }
    public function edit($id){
        $data=Company::where('id','=',$id)->first();
        return view('company.edit-company',compact('data'));
    }
    public function update(Request $request, Company $company){


        $company = new Company();
        $company->id= $request->id;
        $company->name = $request->name;
        $company->email=$request->email;
        //------------------------//
        //upload an image
        $logo=$request->logo;
        if ($logo!=null){
            //delete old one
            Storage::delete('logo');
            //upload a new image
            $imagename= $logo->getCLientOriginalName();

            $logo->storeAs('public/images',$imagename);
            $company->logo = $imagename;
        }
        //------------------------//
        $company->website=$request->website;


        Company::where('id','=',$company->id)->update([
            'name' =>$company->name,
            'email'=>$company->email,
            'logo' => $company->logo,
            'website'=> $company->website,
        ]);
        return redirect()->back()->with('success', 'Record Updated Successfully!!!');

    }

    public function delete($id){
        Company::where('id','=',$id)->delete();
        return redirect()->back()->with('success', 'Record Deleted Successfully!!!');
    }
}
